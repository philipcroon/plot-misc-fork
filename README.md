# A collection of plotting functions
__version__: `0.2.0`


This repository collects plotting modules written on top of `matplotlib` or
`seaborn`. 

## Installation 

plot-misc can be installed pip installed to a specific conda environment by 
first activating the desired environment.

```sh
conda activate ENVIRONMENT_NAME
# update your packages using conda
conda env update --file resources/conda_env/conda_update.yml
# install plot-misc 
python -m pip install -e .
```

### Dependencies 

Dependencies are automatically installed using `setup.py`.

## Usage

Please have a look at the examples in 
[resources](https://gitlab.com/SchmidtAF/plot-misc/-/tree/master/resources/examples)
for some possible recipes. 

## Updates

plot-misc applies [Semantic Versioning](https://semver.org/).
Please see the [changelog](./CHANGELOG.md') for version specific updates.

## Contributing

We welcome user contributions in all forms:

- Documentation fixes/updates
- Example notebooks to illustrate features
- Bug fixes

All contributors will be included in the contributors list.


